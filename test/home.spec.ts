import '../src/home';

describe('home', () => {
  it('should display a title', () => {
    expect(document.body.querySelector('h1.title')!.textContent).toBe('Hello world!');
  });

  it('should display a paragraph', () => {
    expect(document.body.querySelector('p.content')!.textContent).toBe('This is a paragraph.');
  });
});
