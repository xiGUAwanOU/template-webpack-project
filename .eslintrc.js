module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    sourceType: 'module',
  },
  plugins: [ '@typescript-eslint/eslint-plugin' ],
  extends: [
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  rules: {
    'array-bracket-spacing': [ 'error', 'always' ],
    'arrow-parens': [ 'error', 'always' ],
    'comma-dangle': [ 'error', 'always-multiline' ],
    'linebreak-style': [ 'error', 'unix' ],
    'max-len': [ 'error', 120 ],
    'no-trailing-spaces': [ 'error' ],
    'object-curly-spacing': [ 'error', 'always' ],
    'quote-props': [ 'error', 'consistent' ],
    'quotes': [ 'error', 'single' ],
    'semi': [ 'error', 'always' ],

    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-member-accessibility': [ 'error', { accessibility: 'explicit' } ],
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-var-requires': 'off',
  },
};
