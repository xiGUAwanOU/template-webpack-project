const title = document.createElement('h1');
title.textContent = 'Hello world!';
title.className = 'title';

const content = document.createElement('p');
content.textContent = 'This is a paragraph.';
content.className = 'content';

document.body.appendChild(title);
document.body.appendChild(content);
